docker registry builder
=======================

All docker developers are so busy that they can not make a repository of artifacts. I'm tired. Sharing with you.

#### download url build

    wget -O registry "https://gitlab.com/egeneralov/docker-registry/-/jobs/artifacts/${VERSION}/raw/registry-${VERSION}-${OS}-amd64?job=${OS}"

#### download for mac os

    wget -O registry "https://gitlab.com/egeneralov/docker-registry/-/jobs/artifacts/v2.7.1/raw/registry-v2.7.1-darwin-amd64?job=darwin"

#### download for linux

    wget -O registry "https://gitlab.com/egeneralov/docker-registry/-/jobs/artifacts/v2.7.1/raw/registry-v2.7.1-linux-amd64?job=linux"

